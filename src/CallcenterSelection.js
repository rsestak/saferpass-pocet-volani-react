import React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

class CallcenterSelection extends React.Component {
  componentWillMount() {
    this.state = {
      checked: this.props.callcenters
    }
  }

  componentWillReceiveProps(nextProps) {
    const { callcenters } = nextProps;
    if(this.props.callcenters !== callcenters) {
      const checked = this.props.callcenters.length === 0
                    ? callcenters
                    : this.state.checked.filter(callcenter => callcenters.indexOf(callcenter) > -1);
      this.setState({ checked });
    }
  }

  handleChange = (event, index, checked) => {
    this.props.handleCallcenterSelectionChange(checked);
    this.setState({checked})
  };

  getMenuItems = () => {
    const { checked } = this.state;
    return this.props.callcenters.map((callcenter) => (
      <MenuItem
        key={callcenter}
        insetChildren={true}
        checked={checked && checked.indexOf(callcenter) > -1}
        value={callcenter}
        primaryText={callcenter}
      />
    ));
  }

  render() {
    const { checked } = this.state;
    return this.props.callcenters.length > 0 ?
      <SelectField
        multiple={true}
        floatingLabelText="Callcenter selection"
        floatingLabelFixed={true}
        hintText="Select a callcenter"
        value={checked}
        onChange={this.handleChange}
        style={{width: '100%'}}
      >
        {this.getMenuItems()}
      </SelectField>
      : null
    ;
  }
}

export default CallcenterSelection;
