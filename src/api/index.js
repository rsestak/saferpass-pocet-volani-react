import fetch from 'isomorphic-fetch';

const getData = async () => {
  try {
    const response = await fetch('https://data.gov.sk/api/action/datastore_search?resource_id=c2098147-16f3-41fc-acee-638a31bf3bbf');
    const parsedResponse = await response.json();
    const data = await parsedResponse.result.records;
    return data;
  } catch (error) {
    console.log('error: ', error);
  }
};

export default getData;
