import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getData from './api';
import CallsTable from './CallsTable';
import CallcenterSelection from './CallcenterSelection';

class App extends Component {
  static aggregateData(data) {
    let cityIndex;
    const aggregatedData = data.reduce((prev, curr) => {
      cityIndex = prev.findIndex(record => record.Callcentername === curr.Callcentername);
      if(cityIndex === -1) {
        prev.push({
          Callcentername: curr.Callcentername,
          Identifikovane: parseInt(curr.Identifikovane, 10),
          Neidentifikovane: parseInt(curr.Neidentifikovane, 10),
          Vsetky: parseInt(curr.Vsetky, 10),
          Zdvihnute: parseInt(curr.Zdvihnute, 10),
          Opravnene: parseInt(curr.Opravnene, 10),
          id: curr.Callcentername
        });
      } else {
        prev[cityIndex] = {
          ...prev[cityIndex],
          Identifikovane: prev[cityIndex].Identifikovane + parseInt(curr.Identifikovane, 10),
          Neidentifikovane: prev[cityIndex].Neidentifikovane + parseInt(curr.Neidentifikovane, 10),
          Vsetky: prev[cityIndex].Vsetky + parseInt(curr.Vsetky, 10),
          Zdvihnute: prev[cityIndex].Zdvihnute + parseInt(curr.Zdvihnute, 10),
          Opravnene: prev[cityIndex].Opravnene + parseInt(curr.Opravnene, 10)
        }
      }
      return prev;
    }, []);
    return aggregatedData;
  }

  componentWillMount() {
    let data = JSON.parse(localStorage.getItem('data'));
    if(!data) data = [];
    this.setAppData(data)
  }

  async componentDidMount() {
    let data = await getData();
    if(data) {
      data = App.aggregateData(data);
      localStorage.setItem('data', JSON.stringify(data));
      this.setAppData(data);
    }
  }

  setAppData = (data) => {
    const callcenters = data.map(record => record.Callcentername);
    this.data = data;
    this.setState({ data, callcenters });
  }

  handleCallcenterSelectionChange = (checked) => {
    const data = this.data.filter(record => checked.indexOf(record.Callcentername) > -1);
    this.setState({ data });
  }

  render() {
    const { data, callcenters } = this.state;
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Pocet volani</h2>
        </div>

        <MuiThemeProvider>
          <div>
            <CallsTable data={data} />
            <CallcenterSelection
              callcenters={callcenters}
              handleCallcenterSelectionChange={this.handleCallcenterSelectionChange}
            />
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
