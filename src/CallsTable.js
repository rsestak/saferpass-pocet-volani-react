import React from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

const CallsTable = ({ data }) => {
  const getRows = (data) => {
    return data.map(row => (
      <TableRow key={row.id}>
        <TableRowColumn>{row.Callcentername}</TableRowColumn>
        <TableRowColumn>{row.Identifikovane}</TableRowColumn>
        <TableRowColumn>{row.Neidentifikovane}</TableRowColumn>
        <TableRowColumn>{row.Vsetky}</TableRowColumn>
        <TableRowColumn>{row.Zdvihnute}</TableRowColumn>
        <TableRowColumn>{row.Opravnene}</TableRowColumn>
      </TableRow>
    ))
  }

  return (
    <Table selectable={false}>
      <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
        <TableRow>
          <TableHeaderColumn>Callcentername</TableHeaderColumn>
          <TableHeaderColumn>Identifikovane</TableHeaderColumn>
          <TableHeaderColumn>Neidentifikovane</TableHeaderColumn>
          <TableHeaderColumn>Vsetky</TableHeaderColumn>
          <TableHeaderColumn>Zdvihnute</TableHeaderColumn>
          <TableHeaderColumn>Opravnene</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={false}>
        {!data || data.length === 0
          ? <TableRow>
              <TableRowColumn colSpan="6"
                style={{textAlign: 'center', fontSize: '20px'}}
              >
                No Data To Show
              </TableRowColumn>
            </TableRow>
          : getRows(data)
        }
      </TableBody>
    </Table>
  )
}
export default CallsTable;
